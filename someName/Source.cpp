// the real pretty version
// calculati si afisati pe ecran
// E(a,b,c)= (a+b)/2+c
// E(1,2,1) -> 2.5

#include <iostream>
using namespace std;

int main(){
	int a,b,c;
	float E;

	cout << "a=";
	cin >> a;
	cout << "b=";
	cin >> b;
	cout << "c=";
	cin >> c;

	E=(a+b)/2.0+c;

	cout << "E(" << a << "," << b << "," << c;
	cout << ")=(" << a << "+" << b << ")/2+";
	cout << c << "=" << E;
}